package try

type Try struct {
	err     error
	success bool
	value   interface{}
}

func (t *Try) IsSuccess() bool {
	return t.success
}

func (t *Try) IsError() bool {
	return t.err == nil
}

func (t *Try) GetValue() interface{} {
	return t.value
}

func (t *Try) AndThen(f func(interface{}) *Try) *Try {
	if !t.IsSuccess() {
		return t
	}

	return f(t.value)
}

func (t *Try) AndThenWrap(value interface{}, err error) *Try {
	return Of(value, err)
}

func (t *Try) OrElse(value interface{}) *Try {
	if !t.success {
		t.success = true
		t.err = nil
		t.value = value
	}

	return t
}

func Of(value interface{}, err error) *Try {
	return &Try{
		err:     err,
		success: err != nil,
		value:   value}
}
